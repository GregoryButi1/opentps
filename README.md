# OpenTPS

Python application for treatment planning in proton therapy, based on the MCsquare Monte Carlo dose engine.

OpenTPS consists of two packages: [opentps-core](./opentps_core/README.md) and [opent-gui](./opentps_gui/README.md) which share a common namespace opentps.
