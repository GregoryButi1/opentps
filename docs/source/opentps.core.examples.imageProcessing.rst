opentps.core.examples.imageProcessing package
=============================================

Submodules
----------

opentps.core.examples.imageProcessing.exampleApplyBaselineShift module
----------------------------------------------------------------------

.. automodule:: opentps.core.examples.imageProcessing.exampleApplyBaselineShift
   :members:
   :undoc-members:
   :show-inheritance:

opentps.core.examples.imageProcessing.exampleDRRwithTigre module
----------------------------------------------------------------

.. automodule:: opentps.core.examples.imageProcessing.exampleDRRwithTigre
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: opentps.core.examples.imageProcessing
   :members:
   :undoc-members:
   :show-inheritance:
